/**
 * CalcPrecoPrazoWS.java
 *
 * This file was auto-generated from WSDL
 * by the Apache Axis 1.4 Apr 22, 2006 (06:55:48 PDT) WSDL2Java emitter.
 */

package correios;

public interface CalcPrecoPrazoWS extends javax.xml.rpc.Service {
    java.lang.String getCalcPrecoPrazoWSSoap12Address();

    correios.CalcPrecoPrazoWSSoap_PortType getCalcPrecoPrazoWSSoap12() throws javax.xml.rpc.ServiceException;

    correios.CalcPrecoPrazoWSSoap_PortType getCalcPrecoPrazoWSSoap12(java.net.URL portAddress) throws javax.xml.rpc.ServiceException;
    java.lang.String getCalcPrecoPrazoWSSoapAddress();

    correios.CalcPrecoPrazoWSSoap_PortType getCalcPrecoPrazoWSSoap() throws javax.xml.rpc.ServiceException;

    correios.CalcPrecoPrazoWSSoap_PortType getCalcPrecoPrazoWSSoap(java.net.URL portAddress) throws javax.xml.rpc.ServiceException;
}
