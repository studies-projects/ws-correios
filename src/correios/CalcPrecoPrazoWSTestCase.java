/**
 * CalcPrecoPrazoWSTestCase.java
 *
 * This file was auto-generated from WSDL
 * by the Apache Axis 1.4 Apr 22, 2006 (06:55:48 PDT) WSDL2Java emitter.
 */


package correios;

import static org.junit.Assert.*;

public class CalcPrecoPrazoWSTestCase extends junit.framework.TestCase {
    public CalcPrecoPrazoWSTestCase(java.lang.String name) {
        super(name);
    }

    public void testCalcPrecoPrazoWSSoap12WSDL() throws Exception {
        javax.xml.rpc.ServiceFactory serviceFactory = javax.xml.rpc.ServiceFactory.newInstance();
        java.net.URL url = new java.net.URL(new correios.CalcPrecoPrazoWSLocator().getCalcPrecoPrazoWSSoap12Address() + "?WSDL");
        javax.xml.rpc.Service service = serviceFactory.createService(url, new correios.CalcPrecoPrazoWSLocator().getServiceName());
        assertTrue(service != null);
    }

    public void test1CalcPrecoPrazoWSSoap12CalcPrecoPrazo() throws Exception {
        correios.CalcPrecoPrazoWSSoap12Stub binding;
        try {
            binding = (correios.CalcPrecoPrazoWSSoap12Stub)
                          new correios.CalcPrecoPrazoWSLocator().getCalcPrecoPrazoWSSoap12();
        }
        catch (javax.xml.rpc.ServiceException jre) {
            if(jre.getLinkedCause()!=null)
                jre.getLinkedCause().printStackTrace();
            throw new junit.framework.AssertionFailedError("JAX-RPC ServiceException caught: " + jre);
        }
        assertNotNull("binding is null", binding);

        // Time out after a minute
        binding.setTimeout(60000);

        // Test operation
        correios.CResultado value = null;
        value = binding.calcPrecoPrazo(new java.lang.String(), new java.lang.String(), new java.lang.String(), new java.lang.String(), new java.lang.String(), new java.lang.String(), 0, new java.math.BigDecimal(0), new java.math.BigDecimal(0), new java.math.BigDecimal(0), new java.math.BigDecimal(0), new java.lang.String(), new java.math.BigDecimal(0), new java.lang.String());
        // TBD - validate results
    }

    public void test2CalcPrecoPrazoWSSoap12CalcPrecoPrazoData() throws Exception {
        correios.CalcPrecoPrazoWSSoap12Stub binding;
        try {
            binding = (correios.CalcPrecoPrazoWSSoap12Stub)
                          new correios.CalcPrecoPrazoWSLocator().getCalcPrecoPrazoWSSoap12();
        }
        catch (javax.xml.rpc.ServiceException jre) {
            if(jre.getLinkedCause()!=null)
                jre.getLinkedCause().printStackTrace();
            throw new junit.framework.AssertionFailedError("JAX-RPC ServiceException caught: " + jre);
        }
        assertNotNull("binding is null", binding);

        // Time out after a minute
        binding.setTimeout(60000);

        // Test operation
        correios.CResultado value = null;
        value = binding.calcPrecoPrazoData(new java.lang.String(), new java.lang.String(), new java.lang.String(), new java.lang.String(), new java.lang.String(), new java.lang.String(), 0, new java.math.BigDecimal(0), new java.math.BigDecimal(0), new java.math.BigDecimal(0), new java.math.BigDecimal(0), new java.lang.String(), new java.math.BigDecimal(0), new java.lang.String(), new java.lang.String());
        // TBD - validate results
    }

    public void test3CalcPrecoPrazoWSSoap12CalcPrecoPrazoRestricao() throws Exception {
        correios.CalcPrecoPrazoWSSoap12Stub binding;
        try {
            binding = (correios.CalcPrecoPrazoWSSoap12Stub)
                          new correios.CalcPrecoPrazoWSLocator().getCalcPrecoPrazoWSSoap12();
        }
        catch (javax.xml.rpc.ServiceException jre) {
            if(jre.getLinkedCause()!=null)
                jre.getLinkedCause().printStackTrace();
            throw new junit.framework.AssertionFailedError("JAX-RPC ServiceException caught: " + jre);
        }
        assertNotNull("binding is null", binding);

        // Time out after a minute
        binding.setTimeout(60000);

        // Test operation
        correios.CResultado value = null;
        value = binding.calcPrecoPrazoRestricao(new java.lang.String(), new java.lang.String(), new java.lang.String(), new java.lang.String(), new java.lang.String(), new java.lang.String(), 0, new java.math.BigDecimal(0), new java.math.BigDecimal(0), new java.math.BigDecimal(0), new java.math.BigDecimal(0), new java.lang.String(), new java.math.BigDecimal(0), new java.lang.String(), new java.lang.String());
        // TBD - validate results
    }

    public void test4CalcPrecoPrazoWSSoap12CalcPreco() throws Exception {
        correios.CalcPrecoPrazoWSSoap12Stub binding;
        try {
            binding = (correios.CalcPrecoPrazoWSSoap12Stub)
                          new correios.CalcPrecoPrazoWSLocator().getCalcPrecoPrazoWSSoap12();
        }
        catch (javax.xml.rpc.ServiceException jre) {
            if(jre.getLinkedCause()!=null)
                jre.getLinkedCause().printStackTrace();
            throw new junit.framework.AssertionFailedError("JAX-RPC ServiceException caught: " + jre);
        }
        assertNotNull("binding is null", binding);

        // Time out after a minute
        binding.setTimeout(60000);

        // Test operation
        correios.CResultado value = null;
        value = binding.calcPreco(new java.lang.String(), new java.lang.String(), new java.lang.String(), new java.lang.String(), new java.lang.String(), new java.lang.String(), 0, new java.math.BigDecimal(0), new java.math.BigDecimal(0), new java.math.BigDecimal(0), new java.math.BigDecimal(0), new java.lang.String(), new java.math.BigDecimal(0), new java.lang.String());
        // TBD - validate results
    }

    public void test5CalcPrecoPrazoWSSoap12CalcPrecoData() throws Exception {
        correios.CalcPrecoPrazoWSSoap12Stub binding;
        try {
            binding = (correios.CalcPrecoPrazoWSSoap12Stub)
                          new correios.CalcPrecoPrazoWSLocator().getCalcPrecoPrazoWSSoap12();
        }
        catch (javax.xml.rpc.ServiceException jre) {
            if(jre.getLinkedCause()!=null)
                jre.getLinkedCause().printStackTrace();
            throw new junit.framework.AssertionFailedError("JAX-RPC ServiceException caught: " + jre);
        }
        assertNotNull("binding is null", binding);

        // Time out after a minute
        binding.setTimeout(60000);

        // Test operation
        correios.CResultado value = null;
        value = binding.calcPrecoData(new java.lang.String(), new java.lang.String(), new java.lang.String(), new java.lang.String(), new java.lang.String(), new java.lang.String(), 0, new java.math.BigDecimal(0), new java.math.BigDecimal(0), new java.math.BigDecimal(0), new java.math.BigDecimal(0), new java.lang.String(), new java.math.BigDecimal(0), new java.lang.String(), new java.lang.String());
        // TBD - validate results
    }

    public void test6CalcPrecoPrazoWSSoap12CalcPrazo() throws Exception {
        correios.CalcPrecoPrazoWSSoap12Stub binding;
        try {
            binding = (correios.CalcPrecoPrazoWSSoap12Stub)
                          new correios.CalcPrecoPrazoWSLocator().getCalcPrecoPrazoWSSoap12();
        }
        catch (javax.xml.rpc.ServiceException jre) {
            if(jre.getLinkedCause()!=null)
                jre.getLinkedCause().printStackTrace();
            throw new junit.framework.AssertionFailedError("JAX-RPC ServiceException caught: " + jre);
        }
        assertNotNull("binding is null", binding);

        // Time out after a minute
        binding.setTimeout(60000);

        // Test operation
        correios.CResultado value = null;
        value = binding.calcPrazo(new java.lang.String(), new java.lang.String(), new java.lang.String());
        // TBD - validate results
    }

    public void test7CalcPrecoPrazoWSSoap12CalcPrazoData() throws Exception {
        correios.CalcPrecoPrazoWSSoap12Stub binding;
        try {
            binding = (correios.CalcPrecoPrazoWSSoap12Stub)
                          new correios.CalcPrecoPrazoWSLocator().getCalcPrecoPrazoWSSoap12();
        }
        catch (javax.xml.rpc.ServiceException jre) {
            if(jre.getLinkedCause()!=null)
                jre.getLinkedCause().printStackTrace();
            throw new junit.framework.AssertionFailedError("JAX-RPC ServiceException caught: " + jre);
        }
        assertNotNull("binding is null", binding);

        // Time out after a minute
        binding.setTimeout(60000);

        // Test operation
        correios.CResultado value = null;
        value = binding.calcPrazoData(new java.lang.String(), new java.lang.String(), new java.lang.String(), new java.lang.String());
        // TBD - validate results
    }

    public void test8CalcPrecoPrazoWSSoap12CalcPrazoRestricao() throws Exception {
        correios.CalcPrecoPrazoWSSoap12Stub binding;
        try {
            binding = (correios.CalcPrecoPrazoWSSoap12Stub)
                          new correios.CalcPrecoPrazoWSLocator().getCalcPrecoPrazoWSSoap12();
        }
        catch (javax.xml.rpc.ServiceException jre) {
            if(jre.getLinkedCause()!=null)
                jre.getLinkedCause().printStackTrace();
            throw new junit.framework.AssertionFailedError("JAX-RPC ServiceException caught: " + jre);
        }
        assertNotNull("binding is null", binding);

        // Time out after a minute
        binding.setTimeout(60000);

        // Test operation
        correios.CResultado value = null;
        value = binding.calcPrazoRestricao(new java.lang.String(), new java.lang.String(), new java.lang.String(), new java.lang.String());
        // TBD - validate results
    }

    public void test9CalcPrecoPrazoWSSoap12CalcPrecoFAC() throws Exception {
        correios.CalcPrecoPrazoWSSoap12Stub binding;
        try {
            binding = (correios.CalcPrecoPrazoWSSoap12Stub)
                          new correios.CalcPrecoPrazoWSLocator().getCalcPrecoPrazoWSSoap12();
        }
        catch (javax.xml.rpc.ServiceException jre) {
            if(jre.getLinkedCause()!=null)
                jre.getLinkedCause().printStackTrace();
            throw new junit.framework.AssertionFailedError("JAX-RPC ServiceException caught: " + jre);
        }
        assertNotNull("binding is null", binding);

        // Time out after a minute
        binding.setTimeout(60000);

        // Test operation
        correios.CResultado value = null;
        value = binding.calcPrecoFAC(new java.lang.String(), new java.lang.String(), new java.lang.String());
        // TBD - validate results
    }

    public void testCalcPrecoPrazoWSSoapWSDL() throws Exception {
        javax.xml.rpc.ServiceFactory serviceFactory = javax.xml.rpc.ServiceFactory.newInstance();
        java.net.URL url = new java.net.URL(new correios.CalcPrecoPrazoWSLocator().getCalcPrecoPrazoWSSoapAddress() + "?WSDL");
        javax.xml.rpc.Service service = serviceFactory.createService(url, new correios.CalcPrecoPrazoWSLocator().getServiceName());
        assertTrue(service != null);
    }

    public void test10CalcPrecoPrazoWSSoapCalcPrecoPrazo() throws Exception {
        correios.CalcPrecoPrazoWSSoap_BindingStub binding;
        try {
            binding = (correios.CalcPrecoPrazoWSSoap_BindingStub)
                          new correios.CalcPrecoPrazoWSLocator().getCalcPrecoPrazoWSSoap();
        }
        catch (javax.xml.rpc.ServiceException jre) {
            if(jre.getLinkedCause()!=null)
                jre.getLinkedCause().printStackTrace();
            throw new junit.framework.AssertionFailedError("JAX-RPC ServiceException caught: " + jre);
        }
        assertNotNull("binding is null", binding);

        // Time out after a minute
        binding.setTimeout(60000);

        // Test operation
        correios.CResultado value = null;
        value = binding.calcPrecoPrazo(new java.lang.String(), new java.lang.String(), new java.lang.String(), new java.lang.String(), new java.lang.String(), new java.lang.String(), 0, new java.math.BigDecimal(0), new java.math.BigDecimal(0), new java.math.BigDecimal(0), new java.math.BigDecimal(0), new java.lang.String(), new java.math.BigDecimal(0), new java.lang.String());
        // TBD - validate results
    }

    public void test11CalcPrecoPrazoWSSoapCalcPrecoPrazoData() throws Exception {
        correios.CalcPrecoPrazoWSSoap_BindingStub binding;
        try {
            binding = (correios.CalcPrecoPrazoWSSoap_BindingStub)
                          new correios.CalcPrecoPrazoWSLocator().getCalcPrecoPrazoWSSoap();
        }
        catch (javax.xml.rpc.ServiceException jre) {
            if(jre.getLinkedCause()!=null)
                jre.getLinkedCause().printStackTrace();
            throw new junit.framework.AssertionFailedError("JAX-RPC ServiceException caught: " + jre);
        }
        assertNotNull("binding is null", binding);

        // Time out after a minute
        binding.setTimeout(60000);

        // Test operation
        correios.CResultado value = null;
        value = binding.calcPrecoPrazoData(new java.lang.String(), new java.lang.String(), new java.lang.String(), new java.lang.String(), new java.lang.String(), new java.lang.String(), 0, new java.math.BigDecimal(0), new java.math.BigDecimal(0), new java.math.BigDecimal(0), new java.math.BigDecimal(0), new java.lang.String(), new java.math.BigDecimal(0), new java.lang.String(), new java.lang.String());
        // TBD - validate results
    }

    public void test12CalcPrecoPrazoWSSoapCalcPrecoPrazoRestricao() throws Exception {
        correios.CalcPrecoPrazoWSSoap_BindingStub binding;
        try {
            binding = (correios.CalcPrecoPrazoWSSoap_BindingStub)
                          new correios.CalcPrecoPrazoWSLocator().getCalcPrecoPrazoWSSoap();
        }
        catch (javax.xml.rpc.ServiceException jre) {
            if(jre.getLinkedCause()!=null)
                jre.getLinkedCause().printStackTrace();
            throw new junit.framework.AssertionFailedError("JAX-RPC ServiceException caught: " + jre);
        }
        assertNotNull("binding is null", binding);

        // Time out after a minute
        binding.setTimeout(60000);

        // Test operation
        correios.CResultado value = null;
        value = binding.calcPrecoPrazoRestricao(new java.lang.String(), new java.lang.String(), new java.lang.String(), new java.lang.String(), new java.lang.String(), new java.lang.String(), 0, new java.math.BigDecimal(0), new java.math.BigDecimal(0), new java.math.BigDecimal(0), new java.math.BigDecimal(0), new java.lang.String(), new java.math.BigDecimal(0), new java.lang.String(), new java.lang.String());
        // TBD - validate results
    }

    public void test13CalcPrecoPrazoWSSoapCalcPreco() throws Exception {
        correios.CalcPrecoPrazoWSSoap_BindingStub binding;
        try {
            binding = (correios.CalcPrecoPrazoWSSoap_BindingStub)
                          new correios.CalcPrecoPrazoWSLocator().getCalcPrecoPrazoWSSoap();
        }
        catch (javax.xml.rpc.ServiceException jre) {
            if(jre.getLinkedCause()!=null)
                jre.getLinkedCause().printStackTrace();
            throw new junit.framework.AssertionFailedError("JAX-RPC ServiceException caught: " + jre);
        }
        assertNotNull("binding is null", binding);

        // Time out after a minute
        binding.setTimeout(60000);

        // Test operation
        correios.CResultado value = null;
        value = binding.calcPreco(new java.lang.String(), new java.lang.String(), new java.lang.String(), new java.lang.String(), new java.lang.String(), new java.lang.String(), 0, new java.math.BigDecimal(0), new java.math.BigDecimal(0), new java.math.BigDecimal(0), new java.math.BigDecimal(0), new java.lang.String(), new java.math.BigDecimal(0), new java.lang.String());
        // TBD - validate results
    }

    public void test14CalcPrecoPrazoWSSoapCalcPrecoData() throws Exception {
        correios.CalcPrecoPrazoWSSoap_BindingStub binding;
        try {
            binding = (correios.CalcPrecoPrazoWSSoap_BindingStub)
                          new correios.CalcPrecoPrazoWSLocator().getCalcPrecoPrazoWSSoap();
        }
        catch (javax.xml.rpc.ServiceException jre) {
            if(jre.getLinkedCause()!=null)
                jre.getLinkedCause().printStackTrace();
            throw new junit.framework.AssertionFailedError("JAX-RPC ServiceException caught: " + jre);
        }
        assertNotNull("binding is null", binding);

        // Time out after a minute
        binding.setTimeout(60000);

        // Test operation
        correios.CResultado value = null;
        value = binding.calcPrecoData(new java.lang.String(), new java.lang.String(), new java.lang.String(), new java.lang.String(), new java.lang.String(), new java.lang.String(), 0, new java.math.BigDecimal(0), new java.math.BigDecimal(0), new java.math.BigDecimal(0), new java.math.BigDecimal(0), new java.lang.String(), new java.math.BigDecimal(0), new java.lang.String(), new java.lang.String());
        // TBD - validate results
    }

    public void test15CalcPrecoPrazoWSSoapCalcPrazo() throws Exception {
        correios.CalcPrecoPrazoWSSoap_BindingStub binding;
        try {
            binding = (correios.CalcPrecoPrazoWSSoap_BindingStub)
                          new correios.CalcPrecoPrazoWSLocator().getCalcPrecoPrazoWSSoap();
        }
        catch (javax.xml.rpc.ServiceException jre) {
            if(jre.getLinkedCause()!=null)
                jre.getLinkedCause().printStackTrace();
            throw new junit.framework.AssertionFailedError("JAX-RPC ServiceException caught: " + jre);
        }
        assertNotNull("binding is null", binding);

        // Time out after a minute
        binding.setTimeout(60000);

        // Test operation
        correios.CResultado value = null;
        value = binding.calcPrazo(new java.lang.String(), new java.lang.String(), new java.lang.String());
        // TBD - validate results
    }

    public void test16CalcPrecoPrazoWSSoapCalcPrazoData() throws Exception {
        correios.CalcPrecoPrazoWSSoap_BindingStub binding;
        try {
            binding = (correios.CalcPrecoPrazoWSSoap_BindingStub)
                          new correios.CalcPrecoPrazoWSLocator().getCalcPrecoPrazoWSSoap();
        }
        catch (javax.xml.rpc.ServiceException jre) {
            if(jre.getLinkedCause()!=null)
                jre.getLinkedCause().printStackTrace();
            throw new junit.framework.AssertionFailedError("JAX-RPC ServiceException caught: " + jre);
        }
        assertNotNull("binding is null", binding);

        // Time out after a minute
        binding.setTimeout(60000);

        // Test operation
        correios.CResultado value = null;
        value = binding.calcPrazoData(new java.lang.String(), new java.lang.String(), new java.lang.String(), new java.lang.String());
        // TBD - validate results
    }

    public void test17CalcPrecoPrazoWSSoapCalcPrazoRestricao() throws Exception {
        correios.CalcPrecoPrazoWSSoap_BindingStub binding;
        try {
            binding = (correios.CalcPrecoPrazoWSSoap_BindingStub)
                          new correios.CalcPrecoPrazoWSLocator().getCalcPrecoPrazoWSSoap();
        }
        catch (javax.xml.rpc.ServiceException jre) {
            if(jre.getLinkedCause()!=null)
                jre.getLinkedCause().printStackTrace();
            throw new junit.framework.AssertionFailedError("JAX-RPC ServiceException caught: " + jre);
        }
        assertNotNull("binding is null", binding);

        // Time out after a minute
        binding.setTimeout(60000);

        // Test operation
        correios.CResultado value = null;
        value = binding.calcPrazoRestricao(new java.lang.String(), new java.lang.String(), new java.lang.String(), new java.lang.String());
        // TBD - validate results
    }

    public void test18CalcPrecoPrazoWSSoapCalcPrecoFAC() throws Exception {
        correios.CalcPrecoPrazoWSSoap_BindingStub binding;
        try {
            binding = (correios.CalcPrecoPrazoWSSoap_BindingStub)
                          new correios.CalcPrecoPrazoWSLocator().getCalcPrecoPrazoWSSoap();
        }
        catch (javax.xml.rpc.ServiceException jre) {
            if(jre.getLinkedCause()!=null)
                jre.getLinkedCause().printStackTrace();
            throw new junit.framework.AssertionFailedError("JAX-RPC ServiceException caught: " + jre);
        }
        assertNotNull("binding is null", binding);

        // Time out after a minute
        binding.setTimeout(60000);

        // Test operation
        correios.CResultado value = null;
        value = binding.calcPrecoFAC(new java.lang.String(), new java.lang.String(), new java.lang.String());
        // TBD - validate results
    }

}
