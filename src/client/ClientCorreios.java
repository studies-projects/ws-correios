package example;

import java.net.MalformedURLException;
import java.net.URL;
import java.rmi.RemoteException;

import correios.CResultado;
import correios.CalcPrecoPrazoWSLocator;
import correios.CalcPrecoPrazoWSSoap12Stub;
import correios.CalcPrecoPrazoWSSoap12Stub;
import correios.CalcPrecoPrazoWSSoap_PortType;




/**
 * Created by lilith on 06/06/17.
 */
public class HelloWorldClient {
  public static void main(String[] argv) {

      try {
          CResultado result = new CResultado();
          CResultado result1 = new CResultado();
          

          CalcPrecoPrazoWSLocator locator = new CalcPrecoPrazoWSLocator();
          CalcPrecoPrazoWSSoap12Stub service = new CalcPrecoPrazoWSSoap12Stub(new URL(locator.getCalcPrecoPrazoWSSoapAddress()), null);
          // If authorization is required
//          ((CalcPrecoPrazoWSSoap_BindingStub)service).setUsername("user3");
//          ((CalcPrecoPrazoWSSoap_BindingStub)service).setPassword("pass3");


          result = service.calcPrazo("183740","60840375","60110000");
          result1 = service.calcPrecoFAC("183740","30","10/10/2017");
      } catch (RemoteException ex) {
          ex.printStackTrace();
      } catch (MalformedURLException e) {
          e.printStackTrace();
      }
  }
}
